const { DataTypes } = require("sequelize");
const sequelize = require("../db.js");
const User = require("./Users.js");

const Mensaje = sequelize.define(
  "Mensaje",
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    body: {
      type: DataTypes.STRING,
    },
    from_id:{
        type: DataTypes.INTEGER,
    },
    to_id:{
        type:DataTypes.INTEGER,
    }
  },
  { timestamps: false }
);

module.exports = Mensaje;