const { DataTypes } = require("sequelize");
const sequelize = require("../db.js");

const User = sequelize.define(
  "Users",
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    user_name: {
      type: DataTypes.STRING,
    },
    password: {
      type: DataTypes.STRING,
    },
    tipo_id: {
        type: DataTypes.INTEGER,
    },
    tienda_id: {
        type: DataTypes.INTEGER,
    },
  },
  {
    timestamps: false,
  }
);

module.exports = User;
