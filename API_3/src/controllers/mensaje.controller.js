const Mensaje = require("../models/Mensaje.js");


const createMensaje = async (req, res) => {
    try {
      const { body, from_id, to_id} = req.body;
      if (!body || !from_id || !to_id) {
        return res.status(400).json({ error: "content missing" });
      }
      const mnsjCreate = await Mensaje.create({
        body,
        from_id,
        to_id,
      });
      res.json(mnsjCreate).status(200);
    } catch (error) {
      console.log(error);
    }
  };


const getMensajes = async (req, res) => {
    const AllMnsj = await Mensaje.findAll({});
    res.send(AllMnsj);
};

const getMensajesForId = async(req,res)=>{
    const {id} = req.params;
    const allMnsj = await Mensaje.findAll({
        where:{to_id:id},
    });
    res.json(allMnsj).status(200);
};


module.exports = { createMensaje, getMensajes, getMensajesForId };
