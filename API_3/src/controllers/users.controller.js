const Users = require("../models/Users.js");

const getUsers = async (req, res) => {
  const AllUser = await Users.findAll({});
  res.send(AllUser);
};

const getUser = async (req, res) => {
  const {id} = req.params;
  const user = await Users.findOne({
    where:{id:id}
  });
  res.send(user);
};

const loginUser = async (req, res) => {
  const {user_name,password} = req.body;
  
  const q = await Users.findOne({
    where :{user_name:user_name}
  });
  console.log(q);
  if (!q){
    res.status(200).send({"validated":false});
  }
  else if (q.password==password){
    res.status(200).send({"validated":true,"id":q.id});
  }
  else{
    res.status(200).send({"validated":false});
  }
};

const createUser = async (req, res) => {
  try {
    const { user_name, password, tipo_id, tienda_id } = req.body;
    console.log(tipo_id);
    if (!user_name || !password || !tipo_id) {
      return res.status(400).json({ error: "content missing" });
    }
    const userCreate = await Users.create({
      user_name,
      password,
      tipo_id,
      tienda_id,
    });
    res.json(userCreate).status(200);
  } catch (error) {
    console.log(error);
  }
};

module.exports = { getUsers, loginUser, createUser ,getUser};
