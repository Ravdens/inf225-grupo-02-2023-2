const express = require("express");

const router = express.Router();

const {
  createMensaje,
  getMensajes,
  getMensajesForId,
} = require("../controllers/mensaje.controller.js");

router.get("/mensaje", getMensajes);
router.get("/mensaje/:id", getMensajesForId);
router.post("/mensaje", createMensaje);

module.exports = router;