const login = require("./src/routes/login.js");
const userType = require("./src/routes/userType.js");
const mensaje = require("./src/routes/mensaje.js")
const ind = require("./src/routes/index.js")

module.exports = (app) =>{
    app.use(login);
    app.use(userType);
    app.use(mensaje);
    app.use(ind);
};