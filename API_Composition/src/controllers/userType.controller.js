require('dotenv').config();

const createType = async (req, res) => {
  const response = await fetch(process.env.API3+"type/",{
    method: "POST",
    headers:{
      "Content-type":"application/json",
    },
    body: JSON.stringify(req.body),
  });
  const result = await response.json();
  res.json(result).status(200);
};

const getType = async (req, res) => {
  console.log(process.env.API3+'type/')
  const response = await fetch(process.env.API3+"type/",{
    method: "GET"
  })
  const result = await response.json() 
  //console.log(result);
  res.json(result).status(200);
};

module.exports = { createType, getType };
