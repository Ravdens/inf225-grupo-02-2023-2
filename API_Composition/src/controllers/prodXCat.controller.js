require('dotenv').config();


const linkProductoXCategoria = async (req, res) => {
    const response = await fetch(process.env.API2+"productoXcategoria/",{
        method: "POST",
        headers:{
        "Content-type":"application/json",
        },
        body: JSON.stringify(req.body),
    });
    const result = await response.json();
    res.json(result).status(200);
  
};

const getProductoXCategoria = async (req,res) => {
    const response = await fetch(process.env.API2+"productoXCategoria/",{
      method: "GET"
    })
    const result = await response.json()
    res.json(result).status(200);
};


const getCategoriaByProducto = async(req,res)=>{
    const {id} = req.params;
    const response = await fetch(process.env.API2+"categoria/producto/"+id+"/",{
      method: "GET"
    })
    const result = await response.json()
    res.json(result).status(200);
};

const getProductoByCategoria = async(req,res)=>{
    const {id} = req.params;
    const response = await fetch(process.env.API2+"producto/categoria/"+id+"/",{
      method: "GET"
    })
    const result = await response.json()
    res.json(result).status(200);
};

module.exports = {linkProductoXCategoria,getProductoXCategoria,getProductoByCategoria,getCategoriaByProducto};