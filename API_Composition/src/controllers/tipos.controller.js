require('dotenv').config();

const createTipo = async (req, res) => {
  const response = await fetch(process.env.API1+"tipo/",{
    method: "POST",
    headers:{
      "Content-type":"application/json",
    },
    body: JSON.stringify(req.body),
  });
  const result = await response.json();
  res.json(result).status(200);
};

const getTipo = async (req, res) => {
  console.log(process.env.API1+'tipo/')
  const response = await fetch(process.env.API1+"tipo/",{
    method: "GET"
  })
  const result = await response.json() 
  //console.log(result);
  res.json(result).status(200);
};

module.exports = { createTipo, getTipo };
