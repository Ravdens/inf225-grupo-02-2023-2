require('dotenv').config();

const getUsers = async (req, res) => {
  console.log(process.env.API3+'user/')
  const response = await fetch(process.env.API3+"user/",{
    method: "GET"
  });
  const result = await response.json();
  //console.log(result);
  res.json(result).status(200);
};

const getUser = async (req, res) => {
  const {id} = req.params;
  const response = await fetch(process.env.API3+"user/"+id+"/",{
    method: "GET"
  })
  const result = await response.json()
  res.json(result).status(200);
};

const loginUser = async (req, res) => {
  const response = await fetch(process.env.API3+"user/login",{
    method: "POST",
    headers:{
      "Content-type":"application/json",
    },
    body: JSON.stringify(req.body),
  });
  const result = await response.json();
  res.json(result).status(200);
};

const createUser = async (req, res) => {
  const response = await fetch(process.env.API3+"user/",{
    method: "POST",
    headers:{
      "Content-type":"application/json",
    },
    body: JSON.stringify(req.body),
  });
  const result = await response.json();
  res.json(result).status(200);
};

module.exports = { getUsers,getUser ,loginUser, createUser };
