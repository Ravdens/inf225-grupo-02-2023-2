const express = require('express');
const controller = require('../controllers/index');
const router = new express.Router();

router.get('/createTable', controller.createTable);

module.exports = router;