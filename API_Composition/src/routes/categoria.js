const express = require("express");

const router = express.Router();

const {
  getCategoria,
  createCategoria,
} = require("../controllers/categorias.controller.js");

router.get("/categoria", getCategoria);
router.post("/categoria", createCategoria);

module.exports = router;
