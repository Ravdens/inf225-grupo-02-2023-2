const express = require("express");
const { getTiendas, getTienda, createTienda} = require("../controllers/tiendas.controller.js");

const router = express.Router();

router.get("/tienda", getTiendas);
router.get("/tienda/:id", getTienda);
router.post("/tienda",createTienda)

module.exports = router;
