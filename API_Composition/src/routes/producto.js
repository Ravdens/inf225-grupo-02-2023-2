const express = require("express");
const { getProductos,getProducto,createProducto,getProductosByTienda} = require("../controllers/productos.controller.js");

const router = express.Router();

router.get("/producto", getProductos);
router.get("/producto/:id", getProducto);
router.get("/producto/tienda/:id",getProductosByTienda);
router.post("/producto",createProducto);

module.exports = router;