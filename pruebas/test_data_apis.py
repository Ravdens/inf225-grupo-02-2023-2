import requests as rq
import time
api = "http://localhost:4000/"
api1 = "http://localhost:4001/"
api2 = "http://localhost:4002/"
api3 = "http://localhost:4003/"
apis = [api,api1,api2,api3]


def create_tipo_usuarios():
    # 1=Comprador 2=Emprendedor 3=Gestor
    tipos_de_usuarios = ["Comprador","Emprendedor","Gestor"]
    for tipo in tipos_de_usuarios:
        body = {"nameCategory":tipo}
        resp = rq.post(api+"type",json=body)
        print(resp.text)

def create_usuarios():
    usuarios=[("huevito123","123","1","0"),
        ("xxjimmyxx","123","2","1"),
        ("juanito.peres","123","2","2"),
        ("sapo.sepo","123","3","0"),
        ("Elvis_Presley","321","1","0"),
        ]
    for us in usuarios:
        body = {
            "user_name": us[0],
            "password":us[1],
            "tipo_id":us[2],
            "tienda_id":us[3],
        }
        resp=rq.post(api+"user",json=body)
        print(resp.text)

def get_usuarios():
    resp = rq.get(api+"user/")
    for n in resp.json():
        print(n)

def create_tiendas():
    tiendas=[("Tortas el Jimmy","/../../tiendas/0.jpg",2),
    ("Veysa Manualidades","/../../tiendas/1.jpg",6),]
    for ti in tiendas:
        body = {
            "tienda_name": ti[0],
            "foto":ti[1],
            "owner_id":ti[2],
        }
        resp=rq.post(api+"tienda",json=body)
        print(resp.text)
    
def get_tiendas():
    resp = rq.get(api+"tienda/")
    for n in resp.json():
        print(n)

def create_productos():
    productos=[("Peluche goomy","/../../productos/1.jpg",10000,"Un bonito peluche de Goomy hecho de mocos","Tortas el Jimmy",1),
    ("Decoración torta pascua","/../../productos/3.jpg",20000,"Decoración para poner sobre torta con tematica de pascua","Veysa manualidades",3),
    ("Cupcakes cumpleaños","/../../productos/4.jpg",50000,"Cupcakes personalizados para celebraciones","Bakery Bakeshop",2),]
    for pr in productos:
        body = {
           "name":pr[0],
           "foto":pr[1],
           "costo":pr[2],
           "description":pr[3],
           "tienda_name":pr[4],
           "tienda_id":pr[5], 
        }
        resp=rq.post(api+"producto",json=body)
        print(resp.text)

def get_productos():
    resp = rq.get(api+"producto/")
    for n in resp.json():
        print(n)
    
def create_categorias():
    categorias = ["Manualidades","Comida","Pascua","Navidad"]
    for categoria in categorias:
        body = {"nameCategory":categoria}
        resp = rq.post(api+"categoria/",json=body)
        print(resp.text)

def assign_categorias():
    asigns = [(1,1),(2,1),(2,3),(3,2)]
    for asign in asigns:
        body = {
            "ProductoId":asign[0],
            "CategoriumId":asign[1],
        }
        #print(body)
        resp = rq.post(api+"productoXcategoria/",json=body)
        print(resp.text)

def get_categorias():
    resp = rq.get(api+"categoria/")
    for n in resp.json():
        print,(n)

def get_producto_from_categoria(id):
    categoria = str(id)
    resp = rq.get(api+"producto/categoria/"+categoria+"/")
    for n in resp.json():
        print(n)

def get_categoria_from_producto(id):
    producto = str(id)
    resp = rq.get(api+"categoria/producto/"+producto+"/")
    for n in resp.json():
        print(n)


def get_productoXcategoria():
    resp = rq.get(api+"productoXcategoria/")
    for n in resp.json():
        print(n)

def create_mensajes():
    mensajes=[
        (1,3,"Hola, queria preguntar por los cupcakes. ¿Entregan a Honduras?"),
        (2,3,"¿Los cupcakes pueden ser sin maní? soy alergico al mani"),
        (3,3,"Me gustaría saber si pueden enviar los cupcake por correo"),
        (4,2,"¿Que tan largo puede ser el texto en los cupcake? No se si quepa mi nombre"),
    ]
    for txt in mensajes:
        body = {
            "body": txt[2],
            "from_id":txt[0],
            "to_id":txt[1],
        }
        resp=rq.post(api+"mensaje/",json=body)
        print(resp.text)

def get_mensajes():
    resp = rq.get(api+"mensaje/")
    for n in resp.json():
        print(n)


def build():
    create_tipo_usuarios()
    create_usuarios()
    create_tiendas()
    create_productos()
    create_categorias()
    assign_categorias()
    create_mensajes()

def read():
    get_usuarios()
    get_tiendas()
    get_productos()
    get_categorias()
    get_productoXcategoria()
    get_producto_from_categoria(1)
    get_categoria_from_producto(2)
    get_mensajes()

build()
#read()
#time.sleep(3)

#get_usuarios()
#resp = rq.get(api+"user/").json()
#print(resp[0]["user_name"])
#print(type(resp[0]["user_name"]))

#3get_categorias()