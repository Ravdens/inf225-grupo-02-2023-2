const { DataTypes } = require("sequelize");
const sequelize = require("../db.js");
const Producto = require("./Producto.js");
const Categoria = require("./Categoria.js");

const ProdXCat = sequelize.define(
  "ProdXCat",
  {
    link_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
//    prod_id: {
//      type: DataTypes.INTEGER,
//    },
//    cat_id: {
//      type: DataTypes.INTEGER,
//    },
  },
  {
    timestamps: false,
  }
);

Producto.belongsToMany(Categoria,{through: ProdXCat});
Categoria.belongsToMany(Producto,{through: ProdXCat});
module.exports = ProdXCat;
