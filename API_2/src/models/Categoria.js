const { DataTypes } = require("sequelize");
const sequelize = require("../db.js");
const Producto = require("./Producto.js");

const Categoria = sequelize.define(
  "Categoria",
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    nameCategory: {
      type: DataTypes.STRING,
    },
  },
  { timestamps: false }
);

/** 
Tipo.hasMany(Tienda, {
  foreignKey: "tienda_id",
  sourceKey: "id",
});

Tipo.belongsTo(UserType, {
  foreignKey: "tienda_id",
  targetId: "id",
});
*/

module.exports = Categoria;