const Productos = require("../models/Producto.js");

const getProductos = async (req, res) => {
  const Allproductos = await Productos.findAll({});
  res.json(Allproductos).status(200);
};

const createProducto = async (req, res) => {
  try {
    const { name,foto,costo,description,tienda_name,tienda_id} = req.body;
    console.log(tienda_name);
    if (!name ||!foto ||!costo || !description || !tienda_name || !tienda_id) {
      return res.status(400).json({ error: "content missing" });
    }
    const productoCreate = await Productos.create({
      name,
      foto,
      costo,
      description,
      tienda_name,
      tienda_id,
    });
    res.json(productoCreate).status(200);
  } catch (error) {
    console.log(error);
  }
};

const getProducto = async (req, res) => {
  const {id} = req.params;
  const producto = await Productos.findOne({
    where:{id:id},
  });
  res.json(producto).status(200);
};

const getProductosByTienda = async(req,res)=>{
  const {id} = req.params;
  const Allproductos = await Productos.findAll({
    where:{tienda_id:id},
  });
  res.json(Allproductos).status(200);
}

module.exports = { getProductos,createProducto,getProducto,getProductosByTienda};
