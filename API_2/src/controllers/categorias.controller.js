const Categoria = require("../models/Categoria.js");

const createCategoria = async (req, res) => {
  const { nameCategory } = req.body;

  if (!nameCategory) {
    res.status(400);
    return;
  }
  const productoCategoria = await Categoria.create({
    nameCategory,
  });
  res.json(productoCategoria).status(200);
};

const getCategoria = async (req, res) => {
  const categories = await Categoria.findAll();
  res.json(categories).status(200);
};

module.exports = { createCategoria ,getCategoria };
