const ProdXCat = require("../models/ProdXCat.js");
const Producto = require("../models/Producto.js");
const Categoria = require("../models/Categoria.js");


const linkProductoXCategoria = async (req, res) => {
    const { ProductoId,CategoriumId } = req.body;
    console.log("Linking: "+ProductoId+" to "+CategoriumId)
    if (!ProductoId || !CategoriumId) {
      res.status(400);
      return;
    }
    const resp = await ProdXCat.create({
        ProductoId,
        CategoriumId,
    });
    res.json(resp).status(200);
};

const getProductoXCategoria = async (req,res) => {
    const allAsign = await ProdXCat.findAll({});
    res.json(allAsign).status(200);
};


const getCategoriaByProducto = async(req,res)=>{
    const {id} = req.params;
    const allCat = await Producto.findAll({
        //Esto hace un inner join entre Categoria y CategoriaXProducto, lo cual nos deja buscar Categorias por 1 producto asociado
        include:[{
            model:Categoria,
            required:true,
        }],
        where:{id:id},
    });
    res.json(allCat[0]["dataValues"]["Categoria"]).status(200);
};

const getProductoByCategoria = async(req,res)=>{
    const {id} = req.params;
    const allProd = await Categoria.findAll({
        include:[{
            model:Producto,
            require: true,
        }],
        where: {id:id},
    });
   res.json(allProd[0]["dataValues"]["Productos"]).status(200);
};

module.exports = {linkProductoXCategoria,getProductoXCategoria,getProductoByCategoria,getCategoriaByProducto};