const mysql = require('mysql2');
const Sequelize = require('sequelize');

require('dotenv').config();

/*
const database = mysql.createConnection({
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    port: process.env.DB_PORT,
    host:process.env.DB_HOST
});
*/

const sequelize = new Sequelize(process.env.DB_NAME,process.env.DB_USER,process.env.DB_PASSWORD,{
    host: process.env.DB_HOST,
    dialect:"mysql"
});


//module.exports = database;
module.exports = sequelize
