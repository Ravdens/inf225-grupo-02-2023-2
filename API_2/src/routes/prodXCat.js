const express = require("express");
const{
    linkProductoXCategoria,
    getProductoXCategoria,
    getCategoriaByProducto,
    getProductoByCategoria,
} = require("../controllers/prodXCat.controller.js");
const router = express.Router();

router.get("/productoXcategoria",getProductoXCategoria);
router.get("/producto/categoria/:id",getProductoByCategoria);
router.get("/categoria/producto/:id",getCategoriaByProducto);
router.post("/productoXcategoria",linkProductoXCategoria);

module.exports = router;