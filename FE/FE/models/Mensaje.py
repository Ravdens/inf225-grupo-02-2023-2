from reflex import Base

class Mensaje(Base):
    from_id: int = -1
    to_id: int = -1
    from_str: str = "null"
    to_str: str = "null"
    body: str = "null"

    def create(self,from_id,to_id,from_str,to_str,body):
        self.from_id = from_id
        self.to_id = to_id
        self.from_str=from_str
        self.to_str=to_str
        self.body=body