from reflex import Base

class Usuario(Base):
    u_id : int = -1
    u_name : str = "null"
    password : str = "null"
    # 1=Comprador 2=Emprendedor 3=Gestor
    tipo : int = 0

    def create(self,u_id,u_name,password,tipo):
        self.u_id = u_id
        self.u_name = u_name
        self.password = password
        self.tipo = tipo