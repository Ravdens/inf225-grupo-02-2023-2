import reflex as rx
from FE.state import State
from ..models.Producto import Producto

#Version nueva
def foto_marco(producto: dict)->rx.button:
    return rx.button(
        rx.center(
            rx.image(src=producto["foto"],width="128px",height="128px",align="bottom"),            
            bg="lightblue",
            border_radius="xl",
            width="150px",
            height="150px",
        ),
        on_click=rx.redirect("/producto/"+str(producto["id"])),
        variant="unstyled",
        width="150px",
        height="150px",
    )

'''
#Version antigua
def foto_marco(producto: Producto())->rx.button:
    return rx.button(
        rx.center(
            rx.image(src=producto.foto,width="128px",height="128px",align="bottom"),            
            bg="lightblue",
            border_radius="xl",
            width="150px",
            height="150px",
        ),
        on_click=rx.redirect("/producto/"+producto.l_id_str),
        variant="unstyled",
        width="150px",
        height="150px",
    )

'''