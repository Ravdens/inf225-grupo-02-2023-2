import reflex as rx
from FE.state import State

def login()->rx.modal:
    return rx.modal(
        rx.modal_overlay(
            rx.modal_content(
                rx.modal_header("Iniciar sesión"),
                rx.modal_body(
                    rx.vstack(
                        rx.input(value=State.login_u_name,placeholder="Nombre de usuario" ,
                        on_change=State.set_login_u_name),
                        rx.password(value=State.login_pword,placeholder="Contraseña",
                        on_change=State.set_login_pword),
                    ),
                ),
                rx.modal_footer(
                    rx.hstack(
                        rx.button("Entrar", on_click=State.try_login),
                        rx.button("Cancelar", on_click=State.close_login)
                    ),
                ),
            ),
        ),
        close_on_esc=True,
        is_centered=True,  
        is_open=State.bool_login_modal,
        close_on_overlay_click=True,
    )