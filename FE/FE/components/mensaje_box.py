import reflex as rx
from FE.state import State
from ..models.Mensaje import Mensaje

def mensaje_box(mensaje: dict)->rx.card:
    return rx.card(
        rx.text("   -"+str(mensaje["body"])),
        #TODO: Arreglar
        #header=rx.heading("Desde: "+mensaje.from_str,font_size="sm"),
        header=rx.heading("Desde: XXXX",font_size="sm"),
        footer=rx.button("Responder",on_click=State.open_mensaje()),
        width="110%",
        #height="150px",
        bg="lightgreen",
        border_radius="x2",
        justify="left",
        variant= "filled",
    )       