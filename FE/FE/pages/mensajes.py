import reflex as rx
from ..state import State
from ..components.login import login
from ..components.navbar import navbar
from ..components.mensaje_modal import mensaje_modal
from ..components.mensaje_box import mensaje_box

@rx.page(route='/mensajes/',on_load=State.load_mensajes)
def mensajes()->rx.Component:
    return rx.fragment(
        login(),
        mensaje_modal(),
        navbar(),
        rx.cond(
            State.logged_in,
            rx.vstack(
                rx.heading("Tus mensajes",size="md"),
                rx.responsive_grid(
                    rx.foreach(State.all_mensajes,mensaje_box),
                    spacing="10",
                )
            ),
            rx.text("Inicia sesión para ver tus mensajes",padding="50",justify="center"),
        ),
    )