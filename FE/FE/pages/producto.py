import reflex as rx
from ..state import State
from ..components.login import login
from ..components.navbar import navbar
from ..components.mensaje_modal import mensaje_modal

def concadenar_categorias(cat:list[str])->str:
    if len(cat)==0:
        return "No tiene categorias"
    if len(cat)==1:
        return cat[0]
    if len(cat)>=2:
        st = cat[0]
        for x in range(1,len(cat)):
            st = st+", "+cat[x]
        return st


@rx.page(route='/producto/[producto_id]',on_load=State.load_producto)
def producto() ->rx.Component:
    color = State.page_color_theme
    #color = choice(["lightblue","lightgreen","purple","tomato","orange","yellow"])
    return rx.fragment(
        login(),
        mensaje_modal(),
        navbar(),
        rx.vstack(
            rx.heading(State.current_producto["name"], font_size="1em"),
            rx.hstack(
                rx.center(
                    #Nota: Esta funcion estaba bugeada y fue arreglada en la ultima version de reflex (0.2.9)
                    rx.image(src=State.current_producto["foto"] ,width="500px",height="500px"),
                    #rx.image(src="/productos/1.jpg" ,width="500px",height="500px"),
                    bg=color,
                    width="520px",
                    height="520px"
                ),
                rx.vstack(
                    rx.text(State.current_producto["description"],font_size="1em"),
                    rx.table(rows=[
                        ("Tienda",State.current_producto["tienda_name"]),
                        #TODO: Implementar!!
                      #  ("Categorias",State.current_producto.categorias),
                        #("Categorias",concadenar_categorias(State.current_producto.categorias)),
                        ("Costo",State.current_producto["costo"]),],
                        variant="striped",
                        color_scheme=color,
                        font_size="sm"
                    ),
                    rx.hstack(
                        rx.button("Enviar mensaje",bg=color,on_click=State.open_mensaje_2),
                        rx.button("Ver tienda",bg=color,on_click=rx.redirect("/tienda/"+str(State.current_producto["tienda_id"]))),
                    ),
                    width="40%",
                    spacing="10",
                ),
                spacing="100",
                width="90%",
            ),
            spacing="1.5em",
            font_size="2em",
            padding_top="10",
        ),
    )