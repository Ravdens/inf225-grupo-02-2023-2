import reflex as rx
from ..state import State
from random import choice
from ..components.login import login
from ..components.navbar import navbar
from ..components.mensaje_modal import mensaje_modal
from ..components.foto_marco import foto_marco


@rx.page(route='/tienda/[tienda_id]',on_load=State.load_tienda)
def tienda()->rx.Component:
    color = State.page_color_theme
    return rx.fragment(
        login(),
        mensaje_modal(),
        navbar(),
        rx.vstack(
            rx.hstack(
                rx.center(
                    rx.image(src=State.current_tienda["foto"],width="200px",height="200px"),
                    bg=color,
                    width="220px",
                    height="220px",
                ),
                rx.vstack(
                    rx.heading(State.current_tienda["nombre"],font_size="30"),
                    #TODO Conectar esto
                    #rx.text("Tienda creada por: "+State.current_tienda["owner"]),
                ),
                spacing="1.5em",
            ),
            rx.heading("Redes sociales:",font_size="15"),
            rx.hstack(
                rx.image(src="/../../Facebook_icon.png",width="50px",height="50px"),
                rx.image(src="/../../WhatsApp_icon.png",width="50px",height="50px"),
                rx.image(src="/../../Instagram_icon.png",width="50px",height="50px"),
                spacing="10px",
                justify="center",
                width="50%",
            ),
            rx.heading("Productos:",font_size="15"),
            rx.responsive_grid(rx.foreach(State.productos_t,foto_marco,),
                columns=[7],
                spacing="10%",
                width="75%"
            ),
            spacing="10",
            width="90%",
            padding_top="10",
        )

    )