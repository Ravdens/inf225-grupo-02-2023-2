#Base
from rxconfig import config

#Librerias
import reflex as rx
import requests as rq
from random import choice

#Modulos del proyecto
from FE.state import State
from FE.components.navbar import navbar
from FE.components.login import login
from FE.components.mensaje_modal import mensaje_modal
from FE.components.foto_marco import foto_marco
from FE.pages.producto import producto
from FE.pages.tienda import tienda
from FE.pages.mensajes import mensajes


@rx.page(route='/',on_load=State.load_home)
def index() -> rx.Component:
    return rx.fragment(
        login(),
        mensaje_modal(),
        navbar(),
        rx.vstack(
            rx.heading("Bienvenido a la tienda colaborativa", font_size="2em"),
            rx.hstack(
                rx.text("Buscar productos:",font_size="sm"),
                rx.select(
                    #["Manualidades","Comida","Pascua","Navidad"],
                    State.categorias,
                    placeholder="Todos",
                    on_change=State.set_filtro,
                    #on_change=State.filtrar_productos,
                ),
                rx.button(rx.icon(tag="search"),on_click=State.filtrar_productos),
                spacing="30"
            
            ),
            rx.responsive_grid(rx.foreach(State.productos_home,foto_marco,),
            #rx.responsive_grid(rx.foreach(State.productos_home_class,foto_marco,),
                columns=[7],
                spacing="10",
                width="80%",
            ),
            rx.cond(
                State.home_is_empty,
                rx.text("No se encontró ningún producto en la plataforma que cumpla los criterios",padding=50,justify="center")
            ),
            spacing="1.5em",
            font_size="2em",
            padding_top="10",
        ),
    )

app = rx.App()
app.add_page(index)
app.compile()
