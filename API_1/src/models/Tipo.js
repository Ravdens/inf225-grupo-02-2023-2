const { DataTypes } = require("sequelize");
const sequelize = require("../db.js");
const Tienda = require("./Tienda.js");

const Tipo = sequelize.define(
  "Tipos",
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    nameCategory: {
      type: DataTypes.STRING,
    },
  },
  { timestamps: false }
);

/** 
Tipo.hasMany(Tienda, {
  foreignKey: "tienda_id",
  sourceKey: "id",
});

Tipo.belongsTo(UserType, {
  foreignKey: "tienda_id",
  targetId: "id",
});
*/

module.exports = Tipo;