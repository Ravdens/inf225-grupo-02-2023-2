const { DataTypes } = require("sequelize");
const sequelize = require("../db.js");

const Tienda = sequelize.define(
  "Tiendas",
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    tienda_name: {
      type: DataTypes.STRING,
    },
    foto:{
      type: DataTypes.STRING,
    },
    owner_id: {
        type: DataTypes.INTEGER,
    },
  },
  {
    timestamps: false,
  }
);

module.exports = Tienda;
