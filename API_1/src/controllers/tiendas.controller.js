const Tiendas = require("../models/Tienda.js");

const getTiendas = async (req, res) => {
  const Alltiendas = await Tiendas.findAll({});
  res.json(Alltiendas).status(200);
};

const createTienda = async (req, res) => {
  try {
    const { tienda_name,foto,owner_id} = req.body;
    console.log(tienda_name);
    if (!tienda_name || !foto || !owner_id) {
      return res.status(400).json({ error: "content missing" });
    }
    const tiendaCreate = await Tiendas.create({
      tienda_name,
      foto,
      owner_id,
    });
    res.json(tiendaCreate).status(200);
  } catch (error) {
    console.log(error);
  }
};

const getTienda = async (req, res) => {
  const {id} = req.params;
  const tienda = await Tiendas.findOne({
    where:{id:id},
  });
  res.json(tienda).status(200);
};


module.exports = { getTiendas,createTienda,getTienda};
