require("./src/models/Tienda.js");
require("./src/models/Tipo.js");
const sequelize = require("./src/db.js");
const express = require('express');
const app = express();
require('dotenv').config();
const routes = require('./src/routes/index');

app.use(express.json());
app.use(routes);
require("./routes")(app);

/*
app.listen(process.env.PORT_API, () => {
    console.log('Server running!');
});
*/

const PORT = process.env.PORT_API || 4001;
const main = async()=>{
    while (true) {
        try {
            await sequelize.sync();
            await sequelize.authenticate({force:true})
            console.log("Connection has been established successfully.");
            app.listen(PORT,()=>{
                console.log("\n > API Tiendas up")
                console.log(' > Server running on port 4001\n');
            });
            return
        } catch(error){
            console.error(" > Unable to connect to database. Retrying");
            await new Promise(r=> setTimeout(r,process.env.RETRY_TIME))
        }    
    }
}

main();

module.exports = app;